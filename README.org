* BeOS-r5-GTK
[[./screenshot.png]]
** About
GTK theme based on the old appearance of BeOS R5

Forked from [[https://github.com/B00merang-Project/BeOS-R5][B00merang-Project/BeOS-R5]] by [[https://github.com/Elbullazul/BeOS-R5][Elbullazul]]

Changes:
 - titlebar_bg_color set to @dark_bg_color
 - added xfce-notification-plugin fix
